<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DB;
use Illuminate\Http\Request;

class DiagramController extends BaseController {

    public function get($id=1) {
        $master = DB::connection('mysql2')
                    ->table('diagram')
                    ->where('noid', $id)
                    ->first();

        $resultdetail = DB::connection('mysql2')
                    ->table('diagramdetail')
                    ->where('iddiagram', $id)
                    ->get();

        foreach ($resultdetail as $k => $v) {
            // print_r(json_decode("{colorEmpty: 'grey',colorValue: 'blue',value: 35}", true));
            // die;
            $detail[$v->key] = $v;
            // $detail[$v->key]->json = json_encode($v->json);
        }
                    
        return response()->json([
            'success' => true,
            'message' => 'Successfully get data.',
            'data' => [
                'master' => $master,
                'detail' => $detail,
            ]
        ]);
    }

    public function save(Request $request) {
        $id=1;
        $data = $request->get('data');
        $masterjson = $request->get('masterjson');
        $key = '';
        $json = [];

        foreach($data as $k => $v) {
            if ($v['name'] == 'key') {
                $key = $v['value'];
            } else {
                $json[$v['name']] = $v['value'];
            }
        }
        $json = json_encode($json);

        $resultnextnoidmaster = DB::connection('mysql2')
                            ->table('diagram')
                            ->select('noid')
                            ->orderBy('noid','desc')
                            ->limit(1)
                            ->first();
        $resultnextnoiddetail = DB::connection('mysql2')
                            ->table('diagramdetail')
                            ->select('noid')
                            ->orderBy('noid','desc')
                            ->limit(1)
                            ->first();
        $nextnoidmaster = $resultnextnoidmaster ? $resultnextnoidmaster->noid+1 : 1;
        $nextnoiddetail = $resultnextnoiddetail ? $resultnextnoiddetail->noid+1 : 1;

        $form = [
            'noid' => $nextnoiddetail,
            'iddiagram' => $id,
            'key' => $key,
            'json' => $json,
        ];

        $checkmaster = DB::connection('mysql2')
                    ->table('diagram')
                    ->where('noid',$id)
                    ->first();
        $checkdetail = DB::connection('mysql2')
                    ->table('diagramdetail')
                    ->where('iddiagram',$id)
                    ->where('key',$key)
                    ->first();
        // dd($checkdetail);

        if ($checkmaster && $checkdetail) {
            $insertmaster = DB::connection('mysql2')
                                ->table('diagram')
                                ->where('noid',$checkmaster->noid)
                                ->update(['json'=>$masterjson]);
            $insertdetail = DB::connection('mysql2')
                                ->table('diagramdetail')
                                ->where('iddiagram',$checkmaster->noid)
                                ->where('key',$key)
                                ->update($form);
        } else {
            $insertdetail = DB::connection('mysql2')
                                ->table('diagramdetail')
                                ->insert($form);
        }
        
        
        return response()->json([
            'success' => true,
            'message' => 'Successfully insert data.'
        ]);
    }
}
